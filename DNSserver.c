/**
	OBSERVAÇÕES:
		* Estou usando a versão UDP do servidor pois o DNS é majoritariamente
		  construído sobre o UDP
		* Como está na especificação, as mensagens do cliente conterão o IP do
		  cliente concatenado com o recusos, onde o recurso será associado ao IP
		  da mensagem. De um ponto de vista de sockets é desnecessário enviar o
		  IP na mensagem pois o socket precisara estar lincado com o IP do cliente
		  que poderia ser acessado do servidor. De um ponto de vista de segurança
		  em redes, essa aboradagem é bastante insegura pois o cliente pode muito
		  mais facilmente mentir seu IP. No entanto, seguiremos a especificação. 
	
*/
#include "DNSheader.h"

//Constantes
#define THREADS_NUMBER 4			// quantidade de threads reais servindo no thread pooling
#define DNS_FILE "resources.txt"	// arquivo com recursos
#define MICROSEC_TO_SLEEP 10000000

void escutaCliente();
void atendeCliente(struct sockaddr_in client_addr);
void atendeConsulta(struct sockaddr_in client_addr);
void iniciaServidor();
void fechaServidor();

//Variáveis globais 
int serversocket;
struct sockaddr_in servidor_addr;
threadpool thpool;

char buffer[BUFFER_SIZE];

sem_t buff;
sem_t file;

//recebe mensagem do cliente
void escutaCliente(){	
	struct sockaddr_in client_addr;
	int sock_size = sizeof(client_addr);
//	
	sem_wait(&buff);
	if (recvfrom(serversocket, buffer, sizeof(buffer), 0, (struct sockaddr *)&client_addr, &sock_size)<0){
		perror("Erro ao receber mensagem do cliente: ");
		sem_post(&buff);
		thpool_add_work(thpool, (void*)escutaCliente, NULL);
		
	}else{
		if(strcmp(buffer, "consulta") == 0){
			sem_post(&buff);
			printf("Iniciando atendimento de consulta por recursos\n");
			thpool_add_work(thpool, (void*)escutaCliente, NULL);
			atendeConsulta(client_addr);
		}
		else{
			printf("Received buffer: %s\n", buffer);
			thpool_add_work(thpool, (void*)escutaCliente, NULL);
			atendeCliente(client_addr);
		}
	}
}

void atendeConsulta(struct sockaddr_in client_addr){
	int sock_size = sizeof(client_addr);
	// Espera requisição de consulta
	FILE* DNS = fopen(DNS_FILE, "r");
	char* resources = (char*) malloc(sizeof(char) * 4096);
	char line[512];
	while(fgets(line, 512, DNS)!=NULL){
		resources = strcat (resources, line);	
	}
	if(sendto(serversocket, resources, strlen(resources), 0, (struct sockaddr *)&client_addr, sizeof(struct sockaddr))<0){
		perror("Erro ao enviar recursos para requisição de consulta: ");
	}
	else{
		printf("Requisição de consulta de recursos atendida\n");
	}
	fclose(DNS);
}
//atende mensagem do cliente
void atendeCliente(struct sockaddr_in client_addr){
	
	char* IP = strtok(buffer, ":");
	char* res = strtok(NULL, ":");
//
	sem_post(&buff);
	
	printf("Adicionando recurso %s do IP %s: ...\n", res, IP);
	char* newEntry = strcat(res, ":");
	newEntry = strcat(newEntry, IP);
	newEntry = strcat(newEntry, "\n");
	sem_wait(&file);
	FILE* DNS = fopen(DNS_FILE, "a");
	if (fprintf(DNS, "%s", newEntry) == strlen(newEntry)){
		fclose(DNS);	
		sem_post(&file);	
		printf("Sucesso\n");
		char* ANS = "ACK";
		while (sendto(serversocket, ANS, strlen(ANS), 0, (struct sockaddr *)&client_addr, sizeof(struct sockaddr))<0)
		{
			//envia ACK até que envio funcione
		}
	}else{
		fclose(DNS);
		sem_post(&file);
		printf("Falha ao inserir recurso\n");
		char* ANS = "ERR";
		while (sendto(serversocket, ANS, strlen(ANS), 0, (struct sockaddr *)&client_addr, sizeof(struct sockaddr))<0)
		{
			//envia ERR até que envio funcione
		}
	}
	
	
}

void iniciaServidor(){
	printf("Iniciando o servidor DNS. \n");
	//Criando socket cliente
	if ((serversocket = socket(AF_INET,SOCK_DGRAM,0))<0){
		perror("Erro ao criar socket servidor: ");
	}

	printf("Iniciando thread pooling com %d threads\n", THREADS_NUMBER);
	thpool = thpool_init(THREADS_NUMBER);

	//Prepara o endereco do servidor
	servidor_addr.sin_family = AF_INET;
	servidor_addr.sin_addr.s_addr   = INADDR_ANY;
	servidor_addr.sin_port   = htons(SERVER_PORT);
	memset(&(servidor_addr.sin_zero), '\0', sizeof(servidor_addr.sin_zero));

	//Faz a ligacao do socket servidor ao endereco do servidor
	if (bind(serversocket, (struct sockaddr *)&servidor_addr, sizeof(struct sockaddr))<0){
	  perror("Erro ao fazer ligacao no socket servidor: ");
	}
	sem_init(&file, 0, 1);
	sem_init(&buff, 0, 1);
}

// não precisa do semáforo pois acessa somente para leitura
void printResources(){
	printf("######################################################\n");	
	printf("##### Impressão automática periódica de recursos #####\n");
	printf("######################################################\n");

	FILE* DNS = fopen(DNS_FILE, "r");
	char line[512];
	while(fgets(line, 512, DNS)!=NULL){
		printf("\t%s\n", line);
	}
	fclose(DNS);
		
	printf("######################################################\n");	
	printf("###### Fim da impressão automática dos recursos ######\n");
	printf("######################################################\n");
	printf("\n\n\n");
}

void resourcesPrinter(){
	while(1){
		usleep(MICROSEC_TO_SLEEP);
		printResources();
	}
}

void fechaServidor(){
	close(serversocket);
	printf("Servidor terminou. \n");
	exit(0);
}



int main(){
	iniciaServidor();
	thpool_add_work(thpool, (void*)escutaCliente, NULL);
	thpool_add_work(thpool, (void*)resourcesPrinter, NULL);
	thpool_wait(thpool);
	fechaServidor();
}
