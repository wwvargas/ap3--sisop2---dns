#include "DNSheader.h"

#define CLIENT_PORT 54321 
#define SERVER_IP "127.0.0.1"


/**
	Recebe da linha de comando nome de recurso e IP (opcional) que será enviado
*/

int main(int argc, char* argv[]){

	if(argc != 3 && argc != 2){
		printf("Uso incorreto, chame com:\n\t %s <recurso> <IP>\n", argv[0]);
		exit(-1);
	}

	char* sendbuffer;
	
	int clientsocket;
	int porta = CLIENT_PORT;
	int portaservidor = SERVER_PORT;
	char receivebuffer[4];
	struct sockaddr_in servidor_addr;
	struct sockaddr_in client_addr; 
	int sock_size = sizeof(servidor_addr);

	printf("Iniciando o cliente. \n");

	//Criando socket cliente
	if ((clientsocket = socket(AF_INET,SOCK_DGRAM,0))<0){
	  perror("Erro ao criar socket cliente: ");
	}

	//Prepara o endereco do cliente
	client_addr.sin_family = AF_INET;
	//peer_addr.sin_addr.s_addr   = INADDR_ANY; 
	client_addr.sin_addr.s_addr   = INADDR_ANY;
	client_addr.sin_port   = htons(CLIENT_PORT);
	memset(&(client_addr.sin_zero), '\0', sizeof(client_addr.sin_zero));

	//Faz a ligacao do socket cliente ao endereco do cliente
	if (bind(clientsocket, (struct sockaddr *)&client_addr, sizeof(struct sockaddr))<0){
	  perror("Erro ao fazer ligacao no socket cliente");
	}

	//Prepara o endereco do servidor
	servidor_addr.sin_family = AF_INET;
	servidor_addr.sin_addr.s_addr   = inet_addr(SERVER_IP);
	servidor_addr.sin_port   = htons(SERVER_PORT);
	memset(&(servidor_addr.sin_zero), '\0', sizeof(servidor_addr.sin_zero));

	printf("Recurso a ser enviado: %s\n", argv[1]);
	
	sendbuffer = strcat(argv[2], ":");
	sendbuffer = strcat(sendbuffer, argv[1]);
	sendbuffer = strcat(sendbuffer, ":");
	

	printf("sendbuffer = %s\n", sendbuffer);
	int ack = 0;	
	while(ack == 0){
		//Envia mensagem para o servidor
		if(sendto(clientsocket, sendbuffer, BUFFER_SIZE, 0, (struct sockaddr *) &servidor_addr, sock_size)<0){
			perror("Erro ao enviar mensagem ao servidor");
		}
		//Aguarda a resposta do servidor
		if(recvfrom(clientsocket, receivebuffer, sizeof(receivebuffer), 0, (struct sockaddr *)&servidor_addr, &sock_size)<0){
			perror("Erro ao receber resposta do servidor: ");
		}else{
			if (strcmp(receivebuffer, "ACK") == 0) ack = 1;
		}
	}

	//Consulta
	if(sendto(clientsocket, "consulta", sizeof("consulta"), 0, (struct sockaddr *) &servidor_addr, sock_size)	<0){
		perror("Erro ao consultar recursos do servidor: ");
	}
	else{
		char allResources [4096];
		if(recvfrom(clientsocket, allResources, 4096, 0, (struct sockaddr*)&servidor_addr, &sock_size) < 0){
			perror("Erro ao receber recursos do servidor: ");
		}
		else{
			printf("Recursos recebidos:\n%s", allResources);
		}
	}

	close(clientsocket);
	printf("Cliente terminou. \n");
	exit(0);
}
