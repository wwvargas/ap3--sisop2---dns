#include "stdlib.h"
#include "stdio.h"
#include "sys/socket.h"
#include "sys/types.h"
#include "arpa/inet.h"
#include "netinet/in.h"
#include "sys/un.h"
#include "thpool.h"
#include <unistd.h>
#include <semaphore.h>

#define SERVER_PORT 54320
#define BUFFER_SIZE 512

